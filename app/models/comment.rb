class Comment < ActiveRecord::Base
  belongs_to :post
  belongs_to :user
  validates_presence_of :comment
  validates :comment, length: {maximum: 65535}
end
