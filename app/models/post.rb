class Post < ActiveRecord::Base
  has_attached_file :image, :styles => { :medium => "500x500>" }
  validates_attachment_content_type :image, :content_type => /\Aimage\/.*\Z/
  belongs_to :user
  has_many :comments
  validates_presence_of :title, :body, presence: true
  validates :body, length: {maximum: 65535}
  validates :title, length: {maximum: 255}
end
