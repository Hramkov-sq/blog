require 'spec_helper'

describe Post do

  it { should belong_to (:user) }
  it { should have_many (:comments) }
  it { should validate_presence_of (:title) }
  it { should validate_presence_of (:body) }
  it { should validate_length_of(:title).is_at_most(255) }
  it { should validate_length_of(:body).is_at_most(65535) }
end

